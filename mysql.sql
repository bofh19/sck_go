create database test_go;

# For each database:
ALTER DATABASE test_go CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;
# For each table:
#ALTER TABLE table_name CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
# For each column:
#ALTER TABLE table_name CHANGE column_name column_name VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
# (Don’t blindly copy-paste this! The exact statement depends on the column type, maximum length, and other properties. The above line is just an example for a `VARCHAR` column.)


use test_go;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_key` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `family_name` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `given_name` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `user_email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




SELECT * FROM user;

CREATE TABLE user_relations(
	user1 int(11) NOT NULL,
    user2 int(11) NOT NULL,
    FOREIGN KEY (user1) REFERENCES user(id),
    FOREIGN KEY (user2) REFERENCES user(id),
    blocked TINYINT(1) DEFAULT 0,
    accepted TINYINT(1) DEFAULT 0, -- as we have two entries for each relations, this stores the state of other relation
    invited_by INT(11) NOT NULL,
    FOREIGN KEY(invited_by) REFERENCES user(id),
    auth_key varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    secret_key_exchanged TINYINT(1) DEFAULT 0,
    created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_on TIMESTAMP NULL,
    created_by int(11),
    updated_by int(11)
)


select u.* from user u 
inner join user_relations ur on u.id = ur.user2
and ur.user1 = ? and ur.auth_key = ?;

select * from user_relations;

update user_relations set relation_auth_key=NULL where user1=34 and user2=43;
delete from user_relations where user1=34 and user2=43;

INSERT INTO user_relations (`user1`, `user2`, `requested_by`, `requested_on`, `accepted_on`, `created_on`, `updated_on`, `created_by`, `updated_by`) 
VALUES ('34', '43', '34', '2016-4-18', '2016-4-18 00:00:00', '2016-4-18 00:00:00', '2016-4-18 00:00:00', '34', '43');

select u.Id as 'user_id', uf.blocked as 'blocked', uf.accepted, uf.invited_by, u.family_name, u.name, 
u.given_name, uf.auth_key, u.email  from user_relations  uf INNER join user u on u.id = uf.user2 
where user1 = 2 ;

select u.Id as 'user_id', uf.user1_blocked as 'blocked', u.family_name, u.name, 
u.given_name from user_relations  uf INNER join user u on u.id = uf.user2 
where user1 = '34'
UNION  
select u.Id as 'user_id', uf.user2_blocked as 'blocked', u.family_name, 
u.name, u.given_name from user_relations  uf INNER 
join user u on u.id = uf.user1 where user2 = '34';



