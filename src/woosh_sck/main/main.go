package main

import (
	"fmt"
	"woosh_sck/frontend"
	"woosh_sck/wscksrv"
)

func main() {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered ", r)
		}
	}()
	go frontend.RunFrontEnd()
	wscksrv.StartServer(":6070")
}
