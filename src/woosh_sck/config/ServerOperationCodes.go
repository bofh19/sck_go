package config

import ()

const (
	Operation             string = "operation"
	User_message          string = "user_message"
	To_user               string = "to_user"
	Hash_key              string = "hash_key"
	Ping                  string = "ping"
	Pong                  string = "pong"
	Friends_list          string = "friends_list"
	Friend                string = "friend"
	User_message_received string = "user_message_received"
	User_message_sent     string = "user_message_sent"
	Contacts_list         string = "contacts_list"
	Contact               string = "contact"
	Profile_pic           string = "profile_pic"
	Filename              string = "filename"
	Profile               string = "profile"
	Os_type               string = "os_type"
	Os_android            string = "os_android"
	Os_version            string = "os_version"
	User_status           string = "user_status"
	User_display_name     string = "user_display_name"
	Os_desktop            string = "os_desktop"
	Add_friend            string = "add_friend"
	Add_friend_response   string = "add_friend_response"
	Add_friend_from_token string = "add_friend_from_token"
	Update_friend         string = "update_friend"
)
