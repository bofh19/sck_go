package wscksrv

import (
	"net"
	//"bufio"
	//"strings"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"strconv"
	"woosh_sck/config"
	"woosh_sck/models/api"
	"woosh_sck/models/db"
	//"io/ioutil"
)

var writeMap = make(map[string]chan string)
var userMessageBacklog = make(map[string][]string)

func StartServer(service string) error {
	tcpAddr, err := net.ResolveTCPAddr("tcp", service)
	if err != nil {
		return err
	}
	listener, err := net.ListenTCP("tcp", tcpAddr)
	if err != nil {
		return err
	}
	for {
		fmt.Println("Waiting for connection")
		conn, err := listener.Accept()
		fmt.Println("Got connection", conn.RemoteAddr().String())
		if err != nil {
			fmt.Println("Got error after accept", err.Error())
			continue
		}
		go handleSocket(conn)
	}
	return nil
}

func handleSocket(conn net.Conn) {
	first_read := true
	var sck_key string
	var sck_user *db.User
	var buf [16]byte
	var data string
	var api_key string
	var length int
	write_chan := make(chan string)
	for {
		data = ""
		api_key = ""
		length = 0

		_, err := conn.Read(buf[0:16])
		if err != nil {
			fmt.Println("api_key read fail", err.Error())
			break
		}
		api_key = string(buf[0:16])
		if first_read { // first time socket has opened
			first_read = false
			sck_key = api_key
			sck_user, err = db.GetUserByApiKey(sck_key)
			if err != nil || sck_user == nil {
				fmt.Println("failed on getting user", err.Error())
				conn.Close()
				break
			}
			fmt.Println("got user", sck_user.ToString())
			writeMap[sck_user.Username] = write_chan          // add chanel to main map, this will be using redis in future
			go handleSocketWrites(write_chan, sck_user, conn) // call socket write handler in a seperate thread
			// sending user backlog messages
			user_backlog := userMessageBacklog[sck_user.Username]
			if user_backlog != nil {
				fmt.Println("User has back log messages", len(user_backlog))
				for _, user_backlog_msg := range user_backlog {
					write_chan <- user_backlog_msg
				}
				delete(userMessageBacklog, sck_user.Username)
			}
		} else {
			if sck_key != api_key {
				fmt.Println("invalid given key ")
				conn.Close()
				break
			}
		}
		_, err = conn.Read(buf[0:10])
		if err != nil {
			fmt.Println("length read fail", err.Error())
			break
		}
		length, err = strconv.Atoi(string(buf[0:10]))
		if err != nil {
			fmt.Println("lenght conv failed", err.Error())
			break
		}
		for i := 0; i < length; {
			buf := make([]byte, length-i)
			n1, err := conn.Read(buf[0:])
			if err != nil {
				fmt.Println("read failed for data ", err.Error())
				break
			}
			i = i + n1
			data = data + string(buf)
		}
		d_data, err := base64.StdEncoding.DecodeString(data)
		if err != nil {
			fmt.Println("failed to decode base64 string", err.Error())
			break
		}
		fmt.Println("api_key", api_key, "length", length, "data", data)
		fmt.Println(string(d_data[:]))

		go handle_data_after_read(d_data, sck_user)
	}
	_ = conn.Close()
	close(write_chan)
	if sck_user != nil && len(sck_user.Username) > 0 {
		delete(writeMap, sck_user.Username)                                                // delete key if exists
		fmt.Println("Socket closed: exiting read func", sck_user.Username, sck_user.Email) // exiting read func
	}
}

/// handle the reads
func handle_data_after_read(d_data []byte, sck_user *db.User) {
	var f interface{}
	err := json.Unmarshal(d_data, &f)
	if err != nil {
		fmt.Println("failed to unmarshal json", err.Error())
		return
	}
	each_oper := f.(map[string]interface{})
	fmt.Println("each_oper", each_oper["operation"])
	if st, ok := each_oper["operation"].(string); ok && st == config.Ping {
		// if ping return pong
		pongOper := api.NewPongOperation()
		bt, _ := json.Marshal(pongOper)
		writeChanel := writeMap[sck_user.Username]
		writeChanel <- base64.StdEncoding.EncodeToString(bt)
	} else if ok && st == config.Friends_list {
		// if friends list is requested
		fmt.Println("Friends list Requested")
		user_friends, err := sck_user.GetUserFriends()
		if err != nil {
			return
		}
		fmt.Println("GoFrriendsListFromDB: Length:", len(user_friends))
		friends_result := api.UserFriendsList{Operation: config.Friends_list, FriendsList: user_friends}
		fmt.Println("GotFriendsList: ", friends_result.ToString())

		bt, _ := json.Marshal(friends_result)
		fmt.Println("friends result json", string(bt))
		writeChanel := writeMap[sck_user.Username]
		res_out := base64.StdEncoding.EncodeToString(bt)
		writeChanel <- res_out
	} else if ok && st == config.User_message {
		// if send message to someone
		fmt.Println("In UserMessage")
		user_message := api.UserMessage{}
		err = json.Unmarshal(d_data, &user_message)
		if err != nil {
			fmt.Println("Error while unmarshaling usermessage received", err.Error())
			return
		}
		to_user, err := db.GetUserById(user_message.ToId)
		if err != nil {
			fmt.Println("User with Id does not exist or some error", err.Error())
			return
		}

		user_message_received := api.UserMessageReceived{SocketMessage: &api.SocketMessage{Operation: config.User_message_received}, Hash: user_message.Hash}
		writeChanel := writeMap[sck_user.Username]
		msg, err := json.Marshal(user_message_received)
		if err != nil {
			fmt.Println("failed to marshal user message received?? continuing", err.Error())
		} else {
			writeChanel <- base64.StdEncoding.EncodeToString(msg)
			fmt.Println("Sent User Message Received to ", sck_user.Id)
		}

		toUserWriteChanel := writeMap[to_user.Username]
		if toUserWriteChanel != nil {
			msg, err = json.Marshal(user_message)
			if err != nil {
				fmt.Println("Error while sending message to ToUser json marshal", err.Error())
			}
			fmt.Println("Sending Message from", sck_user.Id, "to user", user_message.ToId, "Message:", string(msg))
			toUserWriteChanel <- base64.StdEncoding.EncodeToString(msg)

			user_message_sent := api.UserMessageSent{SocketMessage: &api.SocketMessage{Operation: config.User_message_sent}, Hash: user_message.Hash}
			msg, err = json.Marshal(user_message_sent)
			if err != nil {
				fmt.Println("failed to marshal user message sent?? continuing", err.Error())
			} else {
				writeChanel <- base64.StdEncoding.EncodeToString(msg)
				fmt.Println("Sent User Message Sent to ", sck_user.Id)
			}

		} else {
			fmt.Println("User Chanel Does not exist, probably not online", to_user.Id)
			fmt.Println("adding the encoded message to the user backup map")
			msg, err = json.Marshal(user_message)
			if err != nil {
				fmt.Println("Error while sending message to ToUser json marshal", err.Error())
			}
			user_backlog_msg := base64.StdEncoding.EncodeToString(msg)
			user_backlog := userMessageBacklog[to_user.Username]
			if user_backlog != nil {
				userMessageBacklog[to_user.Username] = append(userMessageBacklog[to_user.Username], user_backlog_msg)
			} else {
				var user_backlog_msgs []string
				user_backlog_msgs = append(user_backlog_msgs, user_backlog_msg)
				userMessageBacklog[to_user.Username] = user_backlog_msgs
			}
		}
	} else if ok && st == config.Add_friend {
		// add user friend new
		fmt.Println("Add User Friend called")
		add_user_friend := api.AddUserFriend{}
		err = json.Unmarshal(d_data, &add_user_friend)
		if err != nil {
			fmt.Println("Error while unmarshaling usermessage received", err.Error())
			return
		}
		add_user_friend_resp, uf, err := sck_user.AddUserFriend(add_user_friend)
		if err != nil {
			fmt.Println("add user friend error", err.Error())
			add_user_friend.Operation = config.Add_friend_response
			add_user_friend.AddUserError = err.Error()
			writeChanel := writeMap[sck_user.Username]
			msg, _ := json.Marshal(add_user_friend)
			writeChanel <- base64.StdEncoding.EncodeToString(msg)
		} else {
			add_user_friend_resp.Operation = config.Add_friend_response
			writeChanel := writeMap[sck_user.Username]
			msg, _ := json.Marshal(add_user_friend_resp)
			writeChanel <- base64.StdEncoding.EncodeToString(msg)

			uf_msg, _ := json.Marshal(uf)
			writeChanel <- base64.StdEncoding.EncodeToString(uf_msg)
		}
	} else if ok && st == config.Add_friend_from_token {
		// add user friend from token
		fmt.Println("Add user friend from token called")
		add_user_friend_token := api.AddUserFriendFromToken{}
		err = json.Unmarshal(d_data, &add_user_friend_token)
		if err != nil {
			fmt.Println("Error while unmarshaling add user friend from token", err.Error())
			return
		}

		uf, err := sck_user.MakeUserFriendsByAuthKey(add_user_friend_token.AuthKey)
		writeChanel := writeMap[sck_user.Username]
		if err != nil {
			fmt.Println("failed while adding user as friend by auth token", err.Error())
			add_user_friend_resp := api.AddUserFriend{SocketMessage: &api.SocketMessage{Operation: config.Add_friend_response}, AddUserError: err.Error()}
			msg, _ := json.Marshal(add_user_friend_resp)
			writeChanel <- base64.StdEncoding.EncodeToString(msg)
			return
		} else {
			msg, _ := json.Marshal(uf)
			writeChanel <- base64.StdEncoding.EncodeToString(msg)
			return
		}
		otherGuy, err := db.GetUserByRelationsAuthKey(uf.Id, add_user_friend_token.AuthKey)
		otherGuyChanel := writeMap[otherGuy.Username]
		if otherGuyChanel != nil {
			otherGuyFriends, err := otherGuy.GetUserFriends()
			if err == nil {
				for _, each_friend := range otherGuyFriends {
					if each_friend.Id == sck_user.Id {
						each_friend.SocketMessage.Operation = config.Update_friend
						otherGuyUpdateFriendMsg, err := json.Marshal(uf)
						if err == nil {
							otherGuyChanel <- base64.StdEncoding.EncodeToString(otherGuyUpdateFriendMsg)
						}
						break
					}
				}
			}
		}
	} else {
		fmt.Println(f)
	}
}

func handleSocketWrites(write_chan chan string, sck_user *db.User, conn net.Conn) {
	for {
		writeThis, more := <-write_chan
		if more {
			lengthStr := get_max_string_length(&writeThis)
			writeThis = sck_user.ApiKey + lengthStr + writeThis
			fmt.Println("Writing To: ", sck_user.Username, "Message:", writeThis)
			conn.Write([]byte(writeThis))
		} else {
			break
		}
	}
	fmt.Println("Socket closed: exiting write func", sck_user.Username) // exiting write func
}

func get_max_string_length(st *string) string {
	le := len(*st)
	leStr := strconv.Itoa(le)
	no_of_zeros_missing := config.Max_Length_For_Message - len(leStr)
	if no_of_zeros_missing <= 0 {
		return leStr
	}
	for i := 0; i < no_of_zeros_missing; i++ {
		leStr = "0" + leStr
	}
	return leStr
}
