package db

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"golang.org/x/crypto/bcrypt"
	"woosh_sck/config"
	"woosh_sck/utils"
)

type User struct {
	Id           int64  `json:"id"`
	Username     string `json:"username"`
	UserPassword string `json:"-"`
	ApiKey       string `json:"api_key"`
	FamilyName   string `json:"family_name"`
	Name         string `json:"name"`
	GivenName    string `json:"given_name"`
	Email        string `json:"email"`
}

func (u *User) ToString() string {
	bt, _ := json.Marshal(u)
	return string(bt)
}

func (u *User) encryptPassword() error {
	password := []byte(u.UserPassword)
	hashedPassword, err := bcrypt.GenerateFromPassword(password, bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	u.UserPassword = string(hashedPassword)
	return nil
}

func (u *User) createRandomApiKey() {
	u.ApiKey = utils.RandStringBytesMaskImprSrc(16)
}

func GetUserByEmail(in_email string) (*User, error) {
	db, err := sql.Open(config.DB_server_type, config.DB_server_string)
	defer db.Close()
	err = db.Ping()
	if err != nil {
		return nil, err
	}
	stmtOut, err := db.Prepare("SELECT id, username,api_key, family_name, name, given_name, email FROM user WHERE email = ?")
	if err != nil {
		fmt.Println("user", "GetUser", "Err: ", 2)
		return nil, err
	}
	defer stmtOut.Close()
	var id sql.NullInt64
	var username sql.NullString
	var api_key sql.NullString
	var family_name sql.NullString
	var given_name sql.NullString
	var name sql.NullString
	var email sql.NullString
	err = stmtOut.QueryRow(in_email).Scan(&id, &username, &api_key, &family_name, &name, &given_name, &email)
	if err != nil {
		return nil, err
	}
	if !id.Valid {
		return nil, errors.New("Invalid User Id received")
	}
	user := &User{Id: id.Int64, Username: username.String, ApiKey: api_key.String, FamilyName: family_name.String, Name: name.String, GivenName: given_name.String, Email: email.String}
	return user, nil
}

func GetUserByRelationsAuthKey(user1_id int64, auth_key string) (*User, error) {
	db, err := sql.Open(config.DB_server_type, config.DB_server_string)
	defer db.Close()
	err = db.Ping()
	if err != nil {
		return nil, err
	}
	stmtOut, err := db.Prepare("select u.id, u.username, u.api_key, u.family_name, u.name, u.given_name, u.email from user u inner join user_relations ur on u.id = ur.user2 and ur.user1 = ? and ur.auth_key = ?;")

	if err != nil {
		fmt.Println("user", "GetUserByRelationsAuthKey", "Err: ", 2)
		return nil, err
	}
	defer stmtOut.Close()
	var id sql.NullInt64
	var username sql.NullString
	var api_key sql.NullString
	var family_name sql.NullString
	var given_name sql.NullString
	var name sql.NullString
	var email sql.NullString
	err = stmtOut.QueryRow(user1_id, auth_key).Scan(&id, &username, &api_key, &family_name, &name, &given_name, &email)
	if err != nil {
		return nil, err
	}
	if !id.Valid {
		return nil, errors.New("Invalid User Id received")
	}
	user := &User{Id: id.Int64, Username: username.String, ApiKey: api_key.String, FamilyName: family_name.String, Name: name.String, GivenName: given_name.String, Email: email.String}
	return user, nil
}

func GetUserById(in_user_id string) (*User, error) {
	fmt.Println("in_user_id", in_user_id)
	db, err := sql.Open(config.DB_server_type, config.DB_server_string)
	defer db.Close()
	err = db.Ping()
	if err != nil {
		return nil, err
	}
	stmtOut, err := db.Prepare("SELECT id, username,api_key, family_name, name, given_name, email FROM user WHERE id = ?")
	if err != nil {
		fmt.Println("user", "GetUser", "Err: ", 2)
		return nil, err
	}
	defer stmtOut.Close()
	var id sql.NullInt64
	var username sql.NullString
	var api_key sql.NullString
	var family_name sql.NullString
	var given_name sql.NullString
	var name sql.NullString
	var email sql.NullString
	err = stmtOut.QueryRow(in_user_id).Scan(&id, &username, &api_key, &family_name, &name, &given_name, &email)
	if err != nil {
		return nil, err
	}
	if !id.Valid {
		return nil, errors.New("Invalid User Id received")
	}
	user := &User{Id: id.Int64, Username: username.String, ApiKey: api_key.String, FamilyName: family_name.String, Name: name.String, GivenName: given_name.String, Email: email.String}
	return user, nil
}

func GetUserByApiKey(in_api_key string) (*User, error) {
	fmt.Println("in_api_key", in_api_key)
	db, err := sql.Open(config.DB_server_type, config.DB_server_string)
	defer db.Close()
	err = db.Ping()
	if err != nil {
		return nil, err
	}
	stmtOut, err := db.Prepare("SELECT id, username,api_key, family_name, name, given_name, email FROM user WHERE api_key = ?")
	if err != nil {
		fmt.Println("user", "GetUser", "Err: ", 2)
		return nil, err
	}
	defer stmtOut.Close()
	var id sql.NullInt64
	var username sql.NullString
	var api_key sql.NullString
	var family_name sql.NullString
	var given_name sql.NullString
	var name sql.NullString
	var email sql.NullString
	err = stmtOut.QueryRow(in_api_key).Scan(&id, &username, &api_key, &family_name, &name, &given_name, &email)
	if err != nil {
		return nil, err
	}
	if !id.Valid {
		return nil, errors.New("Invalid User Id received")
	}
	user := &User{Id: id.Int64, Username: username.String, ApiKey: api_key.String, FamilyName: family_name.String, Name: name.String, GivenName: given_name.String, Email: email.String}
	return user, nil
}

func CreateUser(user *User) error {
	user.encryptPassword()
	user.createRandomApiKey()
	db, err := sql.Open(config.DB_server_type, config.DB_server_string)
	defer db.Close()
	err = db.Ping()
	if err != nil {
		return err
	}

	stmtIns, err := db.Prepare("INSERT INTO user (username, user_password, api_key, family_name, name, given_name, email) VALUES ( ? , ?, ?, ?,?,?,?)")
	if err != nil {
		return err
	}
	defer stmtIns.Close()
	_, err = stmtIns.Exec(user.Username, user.UserPassword, user.ApiKey, user.FamilyName, user.Name, user.GivenName, user.Email)
	if err != nil {
		return err
	}
	return nil
}
