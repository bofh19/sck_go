package db

import (
	"database/sql"
	//"errors"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"strconv"
	//"time"
	"woosh_sck/config"
	"woosh_sck/models/api"
	"woosh_sck/utils"
)

func (u *User) GetUserFriends() ([]api.UserFriend, error) {
	fmt.Println("DBGetUserFriends: UserId:", u.Id)
	db, err := sql.Open(config.DB_server_type, config.DB_server_string)
	defer db.Close()
	err = db.Ping()
	if err != nil {
		return nil, err
	}
	stmtOut, err := db.Prepare(`select u.Id as 'user_id', uf.blocked as 'blocked', uf.accepted, uf.invited_by, u.family_name, u.name, 
								u.given_name, uf.auth_key, u.email, uf.secret_key_exchanged  from user_relations  uf INNER join user u on u.id = uf.user2 
								where user1 = ? ;`)
	if err != nil {
		fmt.Println("error while getting user friends ", u.Id, err.Error())
		return nil, err
	}
	defer stmtOut.Close()
	rows, err := stmtOut.Query(strconv.FormatInt(u.Id, 10))
	if err != nil {
		fmt.Println("error while quering", err.Error())
		return nil, err
	}

	var user_friends []api.UserFriend
	// Fetch rows
	for rows.Next() {
		user_friend := api.UserFriend{}

		var user_id sql.NullInt64
		var blocked sql.NullBool
		var accepted sql.NullBool
		var invited_by sql.NullInt64
		var family_name sql.NullString
		var name sql.NullString
		var given_name sql.NullString
		var auth_key sql.NullString
		var email sql.NullString
		var secret_key_exchanged sql.NullBool

		err = rows.Scan(&user_id, &blocked, &accepted, &invited_by, &family_name, &name, &given_name, &auth_key, &email, &secret_key_exchanged)
		user_friend.Id = user_id.Int64
		user_friend.Blocked = blocked.Bool
		user_friend.Accepted = accepted.Bool
		user_friend.InvitedBy = invited_by.Int64
		user_friend.FamilyName = family_name.String
		user_friend.Name = name.String
		user_friend.GivenName = given_name.String
		user_friend.AuthKey = auth_key.String
		user_friend.Email = email.String
		user_friend.SecretKeyExchanged = secret_key_exchanged.Bool

		user_friends = append(user_friends, user_friend)
		fmt.Println("DB added friend", user_friend.ToString())
	}
	if err = rows.Err(); err != nil {
		fmt.Println("some error in rows", err.Error())
		return nil, err
	}
	fmt.Println("DB UserFriendsLength:", len(user_friends))
	return user_friends, nil
}

func (u *User) AddUserFriend(addUserFriend api.AddUserFriend) (*api.AddUserFriend, *api.UserFriend, error) {

	u_other, err := GetUserByEmail(addUserFriend.UserUniqueDetail)
	if err != nil {
		return nil, nil, err
	}
	db, err := sql.Open(config.DB_server_type, config.DB_server_string)
	defer db.Close()
	err = db.Ping()
	if err != nil {
		return nil, nil, err
	}
	randomString := utils.RandStringBytesMaskImprSrc(128)
	stmtIns, err := db.Prepare(`INSERT INTO user_relations(user1, user2, blocked, accepted, invited_by, auth_key, created_on, created_by) 
								VALUES( ?, ?, 0, 0,?, ?, NOW(), ?)`)
	if err != nil {
		return nil, nil, err
	}

	defer stmtIns.Close()
	_, err = stmtIns.Exec(u.Id, u_other.Id, u.Id, randomString, u.Id)
	if err != nil {
		fmt.Println("invalid val ", err)
		return nil, nil, err
	}

	stmtIns2, err := db.Prepare(`INSERT INTO user_relations(user1, user2, blocked, accepted, invited_by, auth_key, created_on, created_by)
								VALUES ( ?, ?, 0, 1,?, ?, NOW(), ?)`)

	if err != nil {
		return nil, nil, err
	}

	defer stmtIns2.Close()

	_, err = stmtIns2.Exec(u_other.Id, u.Id, u.Id, randomString, u.Id)

	if err != nil {
		fmt.Println("invalid val ", err)
		return nil, nil, err
		// TODO Remove the older entry that was created, not using transactions as this is not a bank app :P
	}

	addUserFriend.AuthKey = randomString
	//t1 := time.Now()
	var uf = api.UserFriend{SocketMessage: &api.SocketMessage{Operation: config.Add_friend},
		AuthKey: randomString, Accepted: false, Blocked: false, InvitedBy: u.Id, SecretKeyExchanged: false,
		Id: u_other.Id, FamilyName: u_other.FamilyName, Name: u_other.Name,
		GivenName: u_other.GivenName, Email: u_other.Email}
	return &addUserFriend, &uf, nil
}

func (u *User) MakeUserFriendsByAuthKey(auth_key string) (*api.UserFriend, error) {
	u_other, err := GetUserByRelationsAuthKey(u.Id, auth_key)
	if err != nil {
		return nil, err
	}
	err = User1AcceptsUser2(u.Id, u_other.Id)
	if err != nil {
		return nil, err
	}
	err = SetSecretKeyExchanged(u.Id, u_other.Id)
	var uf = api.UserFriend{SocketMessage: &api.SocketMessage{Operation: config.Add_friend},
		AuthKey: auth_key, Accepted: false, Blocked: false,
		Id: u_other.Id, FamilyName: u_other.FamilyName, Name: u_other.Name,
		GivenName: u_other.GivenName, Email: u_other.Email}
	return &uf, nil
}

func SetSecretKeyExchanged(u1 int64, u2 int64) error {
	db, err := sql.Open(config.DB_server_type, config.DB_server_string)
	defer db.Close()
	err = db.Ping()
	if err != nil {
		return err
	}
	stmtUpdate1, err := db.Prepare(`UPDATE user_relations SET secret_key_exchanged = 1 WHERE user1 = ? AND user2 = ?`)
	if err != nil {
		return err
	}
	defer stmtUpdate1.Close()
	_, err = stmtUpdate1.Exec(u1, u2)
	if err != nil {
		return err
	}
	_, err = stmtUpdate1.Exec(u2, u1)
	if err != nil {
		return err
	}
	return nil
}

func User1AcceptsUser2(user1 int64, user2 int64) error {
	db, err := sql.Open(config.DB_server_type, config.DB_server_string)
	defer db.Close()
	err = db.Ping()
	if err != nil {
		return err
	}
	stmtUpdate, err := db.Prepare(`UPDATE user_relations SET accepted = 1, updated_by = ?, updated_on = NOW() WHERE user1 = ? AND user2 = ? `)
	if err != nil {
		return err
	}
	defer stmtUpdate.Close()
	_, err = stmtUpdate.Exec(user1, user2, user1)
	if err != nil {
		return err
	}
	return nil
}
