package api

import (
	"bytes"
	"encoding/json"
)

type UserFriend struct {
	*SocketMessage
	Id                 int64  `json:"id"`
	Display_Name       string `json:"display_name"`
	FamilyName         string `json:"family_name"`
	Name               string `json:"name"`
	GivenName          string `json:"given_name"`
	Email              string `json:"email"`
	BitmapUrl          string `json:"bitmapUrl"`
	Blocked            bool   `json:"blocked"`
	Accepted           bool   `json:"accepted"`
	InvitedBy          int64  `json:"invitedBy"`
	AuthKey            string `json:"auth_key"`
	SecretKeyExchanged bool   `json:"secretKeyExchanged"`
}

func (uf *UserFriend) ToString() string {
	bt, _ := json.Marshal(uf)
	return string(bt)
}

type UserFriendsList struct {
	Operation   string       `json:"operation"`
	FriendsList []UserFriend `json:"friends_list"`
}

func (ul *UserFriendsList) ToString() string {
	var buffer bytes.Buffer
	for _, each := range ul.FriendsList {
		buffer.WriteString(each.ToString())
	}
	return buffer.String()
}

type UserMessage struct {
	*SocketMessage
	Id            int    `json:"id"`
	Text          string `json:"text"`
	ToId          string `json:"toId"`
	FromId        string `json:"fromId"`
	Hash          string `json:"hash"`
	EncodedBitmap string `json:"encodedBitmap"`
	Deleted       bool   `json:"deleted"`
	Received      bool   `json:"received"`
}

type AddUserFriend struct {
	*SocketMessage
	AuthKey          string `json:"authKey"`
	UserUniqueDetail string `json:"userUniqueDetail"`
	AddUserError     string `json:"addUserError"`
}

type AddUserFriendFromToken struct {
	*SocketMessage
	AuthKey string `json:"authKey"`
	UserId  string `json:"userId"`
}

type UserMessageReceived struct {
	*SocketMessage
	Hash   string `json:"hash"`
	Tstamp string `json:"tstamp"`
}

type UserMessageSent struct {
	*SocketMessage
	Hash   string `json:"hash"`
	Tstamp string `json:"tstamp"`
}

type UserProfile struct {
	Id           int    `json:"id"`
	Display_Name string `json:"display_Name"`
	Email        string `json:"email"`
	BitmapUrl    string `json:"bitmapUrl"`
	User_Status  string `json:"user_Status"`
}

type AddUserFriendResponse struct {
	*SocketMessage
	AddType     string `json:"addType"`
	AddValue    string `json:addValue"`
	Exists      bool   `json:exists"`
	Invited     bool   `json:"invited"`
	RequestSent bool   `json:"requestSent"`
}
