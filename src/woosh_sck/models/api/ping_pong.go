package api

import (
	"woosh_sck/config"
)

func NewPingOperation() *SocketMessage {
	sm := &SocketMessage{Operation: config.Ping}
	return sm
}

func NewPongOperation() *SocketMessage {
	sm := &SocketMessage{Operation: config.Pong}
	return sm
}
