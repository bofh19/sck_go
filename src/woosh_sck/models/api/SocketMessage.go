package api

import ()

type SocketMessage struct {
	Operation  string `json:"operation"`
	Os_type    string `json:"os_type"`
	Os_version string `json:"os_version"`
}
