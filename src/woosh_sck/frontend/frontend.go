package frontend

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"woosh_sck/models/db"
	"woosh_sck/utils"
)

const scopes string = "https://www.googleapis.com/auth/userinfo.profile+https://www.googleapis.com/auth/userinfo.email+https://www.google.com/m8/feeds/"
const G_C_ID string = "405284180877-sepfua7de171m21oguu77rkgh8u7qtai.apps.googleusercontent.com"

const G_C_SECRET string = "jDbsQnmK08kPB_xSmg0XhRYd"

const token_url string = "https://www.googleapis.com/oauth2/v3/token"
const redirect_url string = "http://127.0.0.1:8000/api/auth/2/"

const USER_INFO_URL string = "https://www.googleapis.com/oauth2/v2/userinfo"

const json_mime_type string = "application/json"

func handler_g_oauth_1(w http.ResponseWriter, r *http.Request) {
	fmt.Println("request for handler_g_oauth_1")
	fmt.Fprintf(w, "<html> requuest oauth "+
		"<a href=\"https://accounts.google.com/o/oauth2/auth?redirect_uri=%s&response_type=code&client_id=%s&scope=%s&approval_prompt=force\">"+
		"Click to OAUTH</a></html>", redirect_url, G_C_ID, scopes)
}

type ResponseForAccessTokenRequest struct {
	AccessToken      string `json:"access_token"`
	TokenType        string `json:"token_type"`
	ExpiresIn        int    `json:"expires_in"`
	IDToken          string `json:"id_token"`
	Error            string `json:"error"`
	ErrorDescription string `json:"error_description"`
}

type ResponseForUserInfo struct {
	ID            string `json:"id"`
	Email         string `json:"email"`
	VerifiedEmail bool   `json:"verified_email"`
	Name          string `json:"name"`
	GivenName     string `json:"given_name"`
	FamilyName    string `json:"family_name"`
	Link          string `json:"link"`
	Picture       string `json:"picture"`
	Gender        string `json:"gender"`
	Locale        string `json:"locale"`
}

func handler_g_oauth_2(w http.ResponseWriter, r *http.Request) {
	fmt.Println("request for handler_g_oauth_2")
	var access_token string
	access_token = r.URL.Query().Get("access_token")
	code := r.URL.Query().Get("code")
	if len(access_token) <= 0 && len(code) <= 0 {
		http.Error(w, "invalid values: err 100x1", http.StatusBadRequest)
		return
	}
	if len(code) > 0 && len(access_token) <= 0 {
		fmt.Printf(code)
		access_token_obj, err := get_access_token_from_code(code)
		if err != nil {
			fmt.Println("Error while getting access token ", err.Error())
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		if len(access_token_obj.Error) > 0 || len(access_token_obj.AccessToken) <= 0 {
			fmt.Println(" invalid values", access_token_obj)
			http.Error(w, access_token_obj.ErrorDescription, http.StatusInternalServerError)
			return
		}
		access_token = access_token_obj.AccessToken
	}

	fmt.Println("access token fetched ", access_token)
	user_info, err := get_user_info_from_access_token(access_token)
	if err != nil {
		fmt.Println("error while getting userinfo ", err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	fmt.Println(user_info)

	u, err := db.GetUserByEmail(user_info.Email)
	if err != nil {
		//read failed trying to create
		fmt.Print("error while getting user from db", err.Error())
		u = &db.User{}
		u.Username = utils.RandStringBytesMaskImprSrc(16)
		u.UserPassword = utils.RandStringBytesMaskImprSrc(16)
		u.Email = user_info.Email
		u.FamilyName = user_info.FamilyName
		u.GivenName = user_info.GivenName
		u.Name = user_info.Name
		err = db.CreateUser(u)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return // both create and read failed
		}
		jsres, err := json.Marshal(u)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", json_mime_type)
		w.Write(jsres)
	} else {
		//read success
		fmt.Print("user from db", u)
		jsres, err := json.Marshal(u)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", json_mime_type)
		w.Write(jsres)
	}
}

func get_access_token_from_code(code string) (*ResponseForAccessTokenRequest, error) {
	var result ResponseForAccessTokenRequest
	query_values := url.Values{}
	query_values.Add("code", code)
	query_values.Add("redirect_uri", redirect_url)
	query_values.Add("redirect_uris", "[\"urn:ietf:wg:oauth:2.0:oob\", \"oob\"]")
	query_values.Add("client_id", G_C_ID)
	query_values.Add("scope", "")
	query_values.Add("client_secret", G_C_SECRET)
	query_values.Add("grant_type", "authorization_code")
	encoded_query := query_values.Encode()

	resp, err := http.Post(token_url, "application/x-www-form-urlencoded", strings.NewReader(encoded_query))
	if err != nil {
		fmt.Println("Error in making access token request", err.Error())
		return &result, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("error while reading body from response in getting acecss token", err.Error())
		return &result, err
	}
	fmt.Println("\naccess_token_json", string(body))
	err = json.Unmarshal(body, &result)
	if err != nil {
		fmt.Println("error while convert access token request json to struct", err.Error())
		return &result, err
	}
	return &result, nil
}

func get_user_info_from_access_token(access_token string) (*ResponseForUserInfo, error) {
	access_token = "Bearer " + access_token
	var result ResponseForUserInfo
	req, err := http.NewRequest("GET", USER_INFO_URL, nil)
	if err != nil {
		return &result, err
	}
	req.Header.Add("Host", "www.googleapis.com")
	req.Header.Add("Content-length", "0")
	req.Header.Add("Authorization", access_token)
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return &result, err
	}
	if resp.StatusCode != 200 {
		return &result, errors.New("Invalid status code " + strconv.Itoa(resp.StatusCode))
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return &result, err
	}
	fmt.Println("\nuserinfo ", string(body))
	err = json.Unmarshal(body, &result)
	return &result, err
}

func RunFrontEnd() {
	http.HandleFunc("/auth/1/", handler_g_oauth_1)
	http.HandleFunc("/api/auth/2/", handler_g_oauth_2)
	fmt.Println("Starting Server on 8000")
	http.ListenAndServe(":8000", nil)
}
